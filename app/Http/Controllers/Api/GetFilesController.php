<?php

namespace App\Http\Controllers\Api;

use DateTime, Storage, Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GetFilesController extends Controller
{
    public function show($path)
    {
        if(Storage::exists($path)) {
	        $file = Storage::get($path);	
	        $mime = Storage::getMimetype($path);	
	        return Response::make($file, 200, array(
		        'Content-Type' => $mime,
	        ));
        } else {
	        return "file not found";
	   }
    }
}