<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta charset="utf-8" />
    <title>smlstyle</title>
    <meta name="fb_admins_meta_tag" content="" />
    <link rel="shortcut icon" href="https://static.parastorage.com/client/pfavico.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="https://static.parastorage.com/client/pfavico.ico" type="image/x-icon" />
    <link rel="canonical" href="http://www.smlstyle.com/" />
    <meta http-equiv="X-Wix-Renderer-Server" content="app203.vae.aws" />
    <meta http-equiv="X-Wix-Meta-Site-Id" content="5e7fb5d6-0ee3-4ab5-952f-2c6b1b13d57c" />
    <meta http-equiv="X-Wix-Application-Instance-Id" content="cacf162b-29f2-493c-bfcc-67bc4f4c0434" />
    <meta http-equiv="X-Wix-Published-Version" content="110" />
    <meta http-equiv="etag" content="13b029cd35c8b386f81333f0d0773e70" />
    <meta property="og:title" content="smlstyle" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.smlstyle.com/" />
    <meta property="og:site_name" content="smlstyle" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta id="wixMobileViewport" name="viewport" content="minimum-scale=0.25, maximum-scale=1.2" />
    <script>
    // BEAT MESSAGE
    try {
        window.wixBiSession = {
            initialTimestamp: Date.now(),
            viewerSessionId: 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random() * 16 | 0,
                    v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            })
        };
        (new Image()).src = 'http://frog.wix.com/bt?src=29&evid=3&pn=1&et=1&v=3.0&vsi=' + wixBiSession.viewerSessionId +
            '&url=' + encodeURIComponent(location.href.replace(/^http(s)?:\/\/(www\.)?/, '')) +
            '&isp=1&st=2&ts=0&c=' + wixBiSession.initialTimestamp;
    } catch (e) {}
    // BEAT MESSAGE END
    </script>
    <!-- META DATA -->
    <script type="text/javascript">
    var serviceTopology = {
        "serverName": "app203.vae.aws",
        "cacheKillerVersion": "1",
        "staticServerUrl": "http://static.parastorage.com/",
        "usersScriptsRoot": "http://static.parastorage.com/services/wix-users/2.575.0",
        "biServerUrl": "http://frog.wix.com/",
        "userServerUrl": "http://users.wix.com/",
        "billingServerUrl": "http://premium.wix.com/",
        "mediaRootUrl": "http://static.wixstatic.com/",
        "logServerUrl": "http://frog.wix.com/plebs",
        "monitoringServerUrl": "http://TODO/",
        "usersClientApiUrl": "https://users.wix.com/wix-users",
        "publicStaticBaseUri": "http://static.parastorage.com/services/wix-public/1.171.0",
        "basePublicUrl": "http://www.wix.com/",
        "postLoginUrl": "http://www.wix.com/my-account",
        "postSignUpUrl": "http://www.wix.com/new/account",
        "baseDomain": "wix.com",
        "staticMediaUrl": "http://static.wixstatic.com/media",
        "staticAudioUrl": "http://media.wix.com/mp3",
        "emailServer": "http://assets.wix.com/common-services/notification/invoke",
        "blobUrl": "http://static.parastorage.com/wix_blob",
        "htmlEditorUrl": "http://editor.wix.com/html",
        "siteMembersUrl": "https://users.wix.com/wix-sm",
        "scriptsLocationMap": {
            "wixapps": "http://static.parastorage.com/services/wixapps/2.461.22",
            "tpa": "http://static.parastorage.com/services/tpa/2.1062.0",
            "santa-resources": "http://static.parastorage.com/services/santa-resources/1.0.0",
            "bootstrap": "http://static.parastorage.com/services/bootstrap/2.1229.41",
            "ck-editor": "http://static.parastorage.com/services/ck-editor/1.87.3",
            "it": "http://static.parastorage.com/services/experiments/it/1.37.0",
            "santa": "http://static.parastorage.com/services/santa/1.1006.4",
            "skins": "http://static.parastorage.com/services/skins/2.1229.41",
            "core": "http://static.parastorage.com/services/core/2.1229.41",
            "sitemembers": "http://static.parastorage.com/services/sm-js-sdk/1.31.0",
            "automation": "http://static.parastorage.com/services/automation/1.23.0",
            "web": "http://static.parastorage.com/services/web/2.1229.41",
            "ecommerce": "http://static.parastorage.com/services/ecommerce/1.196.0",
            "hotfixes": "http://static.parastorage.com/services/experiments/hotfixes/1.15.0",
            "langs": "http://static.parastorage.com/services/langs/2.565.0",
            "santa-versions": "http://static.parastorage.com/services/santa-versions/1.410.0",
            "ut": "http://static.parastorage.com/services/experiments/ut/1.2.0"
        },
        "developerMode": false,
        "productionMode": true,
        "staticServerFallbackUrl": "https://sslstatic.wix.com/",
        "staticVideoUrl": "http://video.wixstatic.com/",
        "scriptsDomainUrl": "http://52.68.79.89/",
        "userFilesUrl": "http://static.parastorage.com/",
        "staticHTMLComponentUrl": "http://www.smlstyle.com.usrfiles.com/",
        "secured": false,
        "ecommerceCheckoutUrl": "https://www.safer-checkout.com/",
        "premiumServerUrl": "https://premium.wix.com/",
        "appRepoUrl": "http://assets.wix.com/wix-lists-ds-webapp",
        "digitalGoodsServerUrl": "http://dgs.wixapps.net/",
        "wixCloudBaseDomain": "cloud.wix.com",
        "mailServiceSuffix": "/_api/common-services/notification/invoke",
        "staticVideoHeadRequestUrl": "http://storage.googleapis.com/video.wixstatic.com",
        "publicStaticsUrl": "http://static.parastorage.com/services/wix-public/1.171.0",
        "staticDocsUrl": "http://media.wix.com/ugd"
    };
    var santaModels = true;
    var rendererModel = {
        "metaSiteId": "5e7fb5d6-0ee3-4ab5-952f-2c6b1b13d57c",
        "siteInfo": {
            "documentType": "UGC",
            "applicationType": "HtmlWeb",
            "siteId": "cacf162b-29f2-493c-bfcc-67bc4f4c0434",
            "siteTitleSEO": "smlstyle"
        },
        "clientSpecMap": {
            "14": {
                "type": "public",
                "applicationId": 14,
                "appDefinitionId": "1379f52d-80e7-1a82-a251-2e09049d661c",
                "appDefinitionName": "Online Business Card",
                "instance": "4p4lI12WN9HSTuNQKSPnHEGJ4n_oHUf82l9IfhHhKiQ.eyJpbnN0YW5jZUlkIjoiMTQwZGEyM2QtMTgwMy1jZDA0LWNiYjYtNzlhZmIyMGM0NDY2Iiwic2lnbkRhdGUiOiIyMDE1LTExLTA3VDA0OjM4OjM1LjkzNVoiLCJ1aWQiOm51bGwsImlwQW5kUG9ydCI6IjEwLjE1LjMuMTYzLzQ2NDczIiwidmVuZG9yUHJvZHVjdElkIjpudWxsLCJkZW1vTW9kZSI6ZmFsc2UsInNpdGVPd25lcklkIjoiN2IzYTUyYjUtY2QwZS00ZjQxLWI0YTMtOWJjMGI5YTc1YzY2In0",
                "sectionPublished": true,
                "sectionMobilePublished": false,
                "sectionSeoEnabled": true,
                "widgets": {
                    "1379f664-e8e4-abef-c3be-0e21731f99cb": {
                        "widgetUrl": "http:\/\/obc.appsharp.com\/obc\/widget",
                        "widgetId": "1379f664-e8e4-abef-c3be-0e21731f99cb",
                        "refreshOnWidthChange": true,
                        "mobileUrl": "http:\/\/obc.appsharp.com\/obc\/widget",
                        "published": true,
                        "mobilePublished": true,
                        "seoEnabled": true
                    }
                },
                "appRequirements": {
                    "requireSiteMembers": false
                },
                "installedAtDashboard": true,
                "permissions": {
                    "revoked": true
                }
            },
            "13": {
                "type": "sitemembers",
                "applicationId": 13,
                "collectionType": "Open",
                "smcollectionId": "0c932110-324d-4ca2-bb1f-3fb1f3de4e0a"
            },
            "2": {
                "type": "appbuilder",
                "applicationId": 2,
                "appDefinitionId": "3d590cbc-4907-4cc4-b0b1-ddf2c5edf297",
                "instanceId": "140d8ea6-f0f4-1789-c986-73e6946fdeb3",
                "state": "Initialized"
            },
            "3": {
                "type": "public",
                "applicationId": 3,
                "appDefinitionId": "135c3d92-0fea-1f9d-2ba5-2a1dfb04297e",
                "appDefinitionName": "Wix ShoutOut",
                "instance": "MuBIZFLW6yjgX3uV73l6PoTgoEOrvOCP1qgU1yFoZik.eyJpbnN0YW5jZUlkIjoiY2Y5MjNmODQtYmJlMS00ZmQzLTg3MzUtNWZmYTI1ZDkzMTU2Iiwic2lnbkRhdGUiOiIyMDE1LTExLTA3VDA0OjM4OjM1LjkzNVoiLCJ1aWQiOm51bGwsImlwQW5kUG9ydCI6IjEwLjE1LjMuMTYzLzQ2NDczIiwidmVuZG9yUHJvZHVjdElkIjpudWxsLCJkZW1vTW9kZSI6ZmFsc2UsInNpdGVPd25lcklkIjoiN2IzYTUyYjUtY2QwZS00ZjQxLWI0YTMtOWJjMGI5YTc1YzY2In0",
                "sectionPublished": true,
                "sectionMobilePublished": false,
                "sectionSeoEnabled": true,
                "widgets": {},
                "appRequirements": {
                    "requireSiteMembers": false
                },
                "installedAtDashboard": true,
                "permissions": {
                    "revoked": false
                }
            },
            "4": {
                "type": "public",
                "applicationId": 4,
                "appDefinitionId": "139ef4fa-c108-8f9a-c7be-d5f492a2c939",
                "appDefinitionName": "Wix Smart Actions",
                "instance": "24myJI1C0imIo7XERVP0CiLs_J1M_XKNELOYoSvCyFY.eyJpbnN0YW5jZUlkIjoiMTlkNjYzYTgtYTEzNS00MGU3LWFiMGQtOWU4NjEyZDVjNTU3Iiwic2lnbkRhdGUiOiIyMDE1LTExLTA3VDA0OjM4OjM1LjkzNVoiLCJ1aWQiOm51bGwsImlwQW5kUG9ydCI6IjEwLjE1LjMuMTYzLzQ2NDczIiwidmVuZG9yUHJvZHVjdElkIjpudWxsLCJkZW1vTW9kZSI6ZmFsc2UsImJpVG9rZW4iOiI0N2E5ZDY3ZS1hZmQ2LTBhNTItM2UyMi1iMmVkMDljNjEwMmIiLCJzaXRlT3duZXJJZCI6IjdiM2E1MmI1LWNkMGUtNGY0MS1iNGEzLTliYzBiOWE3NWM2NiJ9",
                "sectionPublished": true,
                "sectionMobilePublished": false,
                "sectionSeoEnabled": true,
                "widgets": {},
                "appRequirements": {
                    "requireSiteMembers": false
                },
                "installedAtDashboard": true,
                "permissions": {
                    "revoked": false
                }
            }
        },
        "premiumFeatures": ["HasDomain", "ShowWixWhileLoading", "AdsFree"],
        "geo": "USA",
        "languageCode": "en",
        "previewMode": false,
        "userId": "7b3a52b5-cd0e-4f41-b4a3-9bc0b9a75c66",
        "siteMetaData": {
            "preloader": {
                "enabled": true
            },
            "adaptiveMobileOn": true,
            "quickActions": {
                "socialLinks": [],
                "colorScheme": "dark",
                "configuration": {
                    "quickActionsMenuEnabled": false,
                    "navigationMenuEnabled": true,
                    "phoneEnabled": false,
                    "emailEnabled": false,
                    "addressEnabled": false,
                    "socialLinksEnabled": false
                }
            },
            "contactInfo": {
                "companyName": "",
                "phone": "",
                "fax": "",
                "email": "",
                "address": ""
            }
        },
        "runningExperiments": {
            "sv_mobilePresetStructure": "new",
            "viewerFonts2015": "new",
            "sv_allowEditingGaps": "new",
            "sv_svgLink": "new",
            "fontsTrackingInViewer": "new",
            "sv_batchUpdateAnchors": "new",
            "sv_sectionsReorganizeExp": "new",
            "sv_blogCategories": "new",
            "sv_appFlows": "new",
            "sv_blogVideoThumbnail": "new"
        }
    };
    var publicModel = {
        "domain": "smlstyle.com",
        "externalBaseUrl": "http:\/\/www.smlstyle.com\/",
        "unicodeExternalBaseUrl": "http:\/\/www.smlstyle.com\/",
        "pageList": {
            "masterPage": ["http:\/\/52.68.79.89\/storage\/sites\/7b3a52_d0b2bde92375cc50bea61e7b18b4d5d2_108.json", "http:\/\/staticorigin.wixstatic.com\/sites\/7b3a52_d0b2bde92375cc50bea61e7b18b4d5d2_108.json.z?v=3", "http:\/\/www.smlstyle.com\/sites\/7b3a52_d0b2bde92375cc50bea61e7b18b4d5d2_108.json.z?v=3", "http:\/\/fallback.wix.com\/wix-html-editor-pages-webapp\/page\/7b3a52_d0b2bde92375cc50bea61e7b18b4d5d2_108.json"],
            "pages": [{
                "pageId": "cjg9",
                "title": "Home",
                "urls": ["http:\/\/52.68.79.89\/storage\/sites\/7b3a52_1a6e9303ae65c686d1eb5230fceb780d_110.json", "http:\/\/staticorigin.wixstatic.com\/sites\/7b3a52_1a6e9303ae65c686d1eb5230fceb780d_110.json.z?v=3", "http:\/\/www.smlstyle.com\/sites\/7b3a52_1a6e9303ae65c686d1eb5230fceb780d_110.json.z?v=3", "http:\/\/fallback.wix.com\/wix-html-editor-pages-webapp\/page\/7b3a52_1a6e9303ae65c686d1eb5230fceb780d_110.json"]
            }, {
                "pageId": "c24vq",
                "title": "Contact",
                "urls": ["http:\/\/static.wixstatic.com\/sites\/7b3a52_96c7f28f358fbedbbd71d8411a584754_1.json.z?v=3", "http:\/\/staticorigin.wixstatic.com\/sites\/7b3a52_96c7f28f358fbedbbd71d8411a584754_1.json.z?v=3", "http:\/\/www.smlstyle.com\/sites\/7b3a52_96c7f28f358fbedbbd71d8411a584754_1.json.z?v=3", "http:\/\/fallback.wix.com\/wix-html-editor-pages-webapp\/page\/7b3a52_96c7f28f358fbedbbd71d8411a584754_1.json"]
            }],
            "mainPageId": "cjg9"
        },
        "timeSincePublish": 91592187,
        "favicon": "",
        "deviceInfo": {
            "deviceType": "Desktop",
            "browserType": "Chrome",
            "browserVersion": 46
        }
    };



    var googleAnalytics = "UA-xxxxxxxx-1";

    var googleRemarketing = "";
    var facebookRemarketing = "";
    var yandexMetrikaData = {};
    </script>
    <meta name="fragment" content="!" />
    <!-- DATA -->
    <script type="text/javascript">
    var adData = {};
    var mobileAdData = {};
    var usersDomain = "https://users.wix.com/wix-users";
    </script>
    <script type="text/javascript">
    var santaBase = '';
    var clientSideRender = true;
    </script>
    <script src="http://52.68.79.89/services/third-party/requirejs/2.1.15/require.min.js"></script>
    <script src="http://52.68.79.89/services/santa-versions/1.410.0/main-r.js"></script>
    <link rel="stylesheet" href="http://52.68.79.89/services/santa-versions/1.410.0/viewer.css">
</head>

<body>
    <div id="SITE_CONTAINER"></div>
    <!-- No Footer -->
</body>

</html>
